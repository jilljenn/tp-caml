type ('a, 'b) jeu = Noeud of ('a * ('a, 'b) jeu) list | Feuille of 'b;;

(* Question 1 *)

let min_max j =
  let rec min_max min max = function
  | Feuille s -> s, Feuille s
  | Noeud l ->
    let f l (a, j) =
      let s, j = min_max max min j in
      ((s, a), j) :: l
    in
    let l = List.sort min (List.fold_left f [] l) in
    match l with
    | [] -> invalid_arg "Aucune action possible"
    | ((h, _), _) :: _ -> h, Noeud l
  in
  snd (min_max (fun a b -> compare b a) compare j)
;;

(* Question 2 *)

let premier n =
  let rec teste i =
    i * i  > n || (n mod i <> 0 && teste (i+2))
  in
  n = 2 || n mod 2 <> 0 && teste 3
;;

let allumettes n =
  let rec allumettes n x y s =
  if n <= 1 then
    Feuille s
  else
    let rec cons x =
      if x <= 0 then
        []
      else if premier x then
        (x, allumettes (n-x) y x (-s)) :: cons (x-1)
      else
        cons (x-1)
    in
    Noeud (cons (min n x))
  in
  allumettes n n n (-1)
;;

(* Question 3 *)

let interactif n =
  let jeu = min_max (allumettes n) in
  let rec boucle n = function
  | Feuille (-1) -> print_endline "Bravo, vous avez gagné !"
  | Feuille _ -> invalid_arg "Score invalide"
  | Noeud [] -> invalid_arg "Aucune action possible"
  | Noeud (((_, a), j) :: _) -> begin
    print_int a;
    print_newline ();
    print_string "Il reste ";
    print_int (n-a);
    print_endline " allumette(s).";
    match j with
    | Feuille 1 -> print_endline "Vous avez (encore) perdu !"
    | Feuille _ -> invalid_arg "Score invalide"
    | Noeud [] -> invalid_arg "Aucune action possible"
    | Noeud l -> begin
      let x = read_int () in
      let j = List.assoc x (List.map (fun ((_, a), b) -> (a, b)) l) in
      boucle (n-a-x) j
    end
  end
  in
  boucle n jeu;; 
