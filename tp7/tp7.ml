let kmp s =
  let n = String.length s in
  let l = Array.make n 0 in
  let rec bord i j =
  if s.[l.(i)] = s.[j] then l.(j) <- 1 + l.(i)
    else if i = 0 then ()
    else bord l.(i-1) j
  in
  for j = 1 to n - 1 do
    bord (j-1) j
  done;
  l
;;
