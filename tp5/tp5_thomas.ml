let initialise n = Array.init n (fun i -> (i, 0))
;;

let rec trouver foret i =
  let pi, _ = foret.(i) in
  if pi = i then i
  else begin
    let ri = trouver foret pi in
    foret.(i) <- (ri, 0);
    ri
  end
;;

let union foret i j =
  let ri = trouver foret i
  and rj = trouver foret j
  in
  ri = rj || begin
    if snd foret.(ri) < snd foret.(rj) then
      foret.(ri) <- (rj, 0)
    else if snd foret.(ri) > snd foret.(rj) then
      foret.(rj) <- (ri, 0)
    else begin
      foret.(ri) <- (ri, snd foret.(ri) + 1);
      foret.(rj) <- (ri, 0)
    end;
    false
  end
;;
