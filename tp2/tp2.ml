let glouton pieces montant = aux pieces montant [] where
	rec aux pieces montant nb_pieces = match pieces with
		| [] -> rev nb_pieces
		| h::t -> aux t (montant mod h) ((montant / h)::nb_pieces)
;;

glouton [4; 3; 1] 6;;
glouton [4; 3; 1] 8;;

let prog_dyn pieces montant =
	let n = vect_length pieces in
	let nb_pieces_min = make_matrix n (montant + 1) 0 in
	for j = 1 to montant do nb_pieces_min.(0).(j) <- j done;
	for i = 1 to n - 1 do
		for j = 1 to pieces.(i) - 1 do nb_pieces_min.(i).(j) <- nb_pieces_min.(i - 1).(j) done;
		for j = pieces.(i) to montant do
			nb_pieces_min.(i).(j) <- min (1 + nb_pieces_min.(i).(j - pieces.(i))) (nb_pieces_min.(i - 1).(j)) 
		done
	done;
	aux (n - 1) montant [] where rec aux i montant nb_pieces = match montant with
		| 0 -> nb_pieces
		| n -> if montant >= pieces.(i) & nb_pieces_min.(i).(montant - pieces.(i)) == nb_pieces_min.(i).(montant) - 1 then aux i (montant - pieces.(i)) (pieces.(i)::nb_pieces) else aux (i - 1) montant nb_pieces
;;

prog_dyn [|1; 3; 4|] 6;;


